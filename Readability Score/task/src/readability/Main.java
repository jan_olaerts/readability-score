package readability;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        for(String parameter : args) System.out.print(parameter + " ");
        System.out.print("\n");
        String text = readInputFile(args);
        List<Integer> data = getTextData(text);
        showData(data);
        getAlgorithm(data);
    }

    public static String readInputFile(String[] args) {
        String fileName = null;
        for(int i = 0; i < args.length; i++) {
            if(args[i].contains(".txt")) fileName = args[i];
        }

        File file = new File("./" + fileName);
//        File file = new File("C:\\Users\\Intel\\Desktop\\" + fileName);

        StringBuilder sb = new StringBuilder();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                sb.append(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error: " + e.getMessage());
        }

        System.out.println("The text is: ");
        System.out.println(sb.toString());
        System.out.println("");

        return sb.toString();
    }

    public static List<Integer> getTextData(String text) {

        String[] sentences = text.split("[\\.!\\?]");
        String[] words = text.split(" ");
        int sentenceCount = sentences.length;
        int charCount = 0;

        char[] characters = text.toCharArray();
        for(int i = 0; i < characters.length; i++) {
            if(characters[i] != ' ' || characters[i] != '\r') charCount++;
        }

        int wordCount = 1;
        ArrayList<Integer> wordCounts = new ArrayList<>();
        for(int i = 0; i < sentences.length; i++) {

            for(int j = 0; j < sentences[i].toCharArray().length; j++) {
                char character = sentences[i].toCharArray()[j];

                if(Character.isWhitespace(character)) charCount--;
                if(character == ' ') wordCount++;

                if(j == sentences[i].toCharArray().length -1) {
                    wordCounts.add(wordCount);
                    wordCount = 0;
                }
            }
        }

        int totalWordCount = 0;
        for(int item : wordCounts) {
            totalWordCount += item;
        }

        int[] syllableCount = getSyllableCount(words);

        return new ArrayList(List.of(totalWordCount, sentenceCount, charCount, syllableCount[0], syllableCount[1]));
    }

    public static int[] getSyllableCount(String[] words) {
        int syllableCount = 0;
        int polySyllableCount = 0;
        List<Character> vowels = List.of('a', 'e', 'i', 'o', 'u', 'y');

        for(String word : words) {
            int syllablesInWord = 0;
            boolean flag = false;

            for(int i = 0; i < word.length(); i++) {
                char currentChar = word.charAt(i);
                if(i == word.length() -1 && currentChar == 'e') break;

                if(vowels.contains(currentChar)) {
                    if (!flag) syllablesInWord++;
                    flag = true;
                } else flag = false;
            }

            syllablesInWord = syllablesInWord > 0 ? syllablesInWord : 1;
            syllableCount += syllablesInWord;
            if(syllablesInWord >= 3) polySyllableCount++;
        }

        return new int[]{syllableCount, polySyllableCount -1};
    }

    public static void getAlgorithm(List<Integer> data) {
        Algorithms al = new Algorithms(data);

        System.out.print("Enter the score you want to calculate (ARI, FK, SMOG, CL, all): ");
        String input = scanner.nextLine();
        System.out.print("\n");
        switch (input) {
            case "ARI":
                al.calculateARI();
                break;
            case "FK":
                al.calculateFK();
                break;
            case "SMOG":
                al.calculateSMOG();
                break;
            case "CL":
                al.calculateCL();
                break;
            case "all":
                double score1 = al.calculateARI();
                double score2 = al.calculateFK();
                double score3 = al.calculateSMOG();
                double score4 = al.calculateCL();
                printAverageAge(al, score1, score2, score3, score4);
                break;
        }
    }

    public static void printAverageAge(Algorithms al, double score1, double score2, double score3, double score4) {
        int age1 = al.calculateAges(score1);
        int age2 = al.calculateAges(score2);
        int age3 = al.calculateAges(score3);
        int age4 = al.calculateAges(score4);

        double avgAge = (double) (age1 + age2 + age3 + age4) / 4d;
        System.out.format("%nThis text should be understood in average by %.2f year olds.", avgAge);
    }

    public static void showData(List<Integer> data) {
        double wordAmount = data.get(0);
        double sentenceAmount = data.get(1);
        double charAmount = data.get(2);
        double syllableAmount = data.get(3);
        double polySyllableAmount = data.get(4);

        System.out.println("Words: " + (int) wordAmount);
        System.out.println("Sentences: " + (int) sentenceAmount);
        System.out.println("Characters: " + (int) charAmount);
        System.out.println("Syllables: " + (int) syllableAmount);
        System.out.println("Polysyllables: " + (int) polySyllableAmount);
    }
}