package readability;

import java.util.ArrayList;
import java.util.List;

public class Algorithms {

    double words;
    double sentences;
    double chars;
    double syllables;
    double polySyllables;

    public Algorithms(List<Integer> data) {
        this.words = data.get(0);
        this.sentences = data.get(1);
        this.chars = data.get(2);
        this.syllables = data.get(3);
        this.polySyllables = data.get(4);
    }

    public double calculateARI() {
        double score = 4.71 * (chars / words) + 0.5 * (words / sentences) - 21.43;
        System.out.format("Automated Readability Index: %.2f (about %d year olds).%n", score, calculateAges(score));
        return score;
    }

    public double calculateFK() {
        double score = 0.39 * (words / sentences) + 11.8 * (syllables / words) - 15.59;
        System.out.format("Flesch–Kincaid readability tests: %.2f (about %d year olds).%n", score, calculateAges(score));
        return score;
    }

    public double calculateSMOG() {
        double score = 1.043 * Math.sqrt(polySyllables * (30 / sentences)) + 3.1291;
        System.out.format("Simple Measure of Gobbledygook: %.2f (about %d year olds).%n", score, calculateAges(score));
        return score;
    }

    public double calculateCL() {
        double l = (chars / words) * 100d;
        double s = (sentences / words) * 100d;

        double score = 0.0588 * l - 0.296 * s - 15.8;
        System.out.format("Coleman–Liau index: %.2f (about %d year olds).%n", score, calculateAges(score));
        return score;
    }

    public int calculateAges(double score) {
        int age = 0;

        score = Math.ceil(score);

        if((int) score == 1) age = 5;
        if((int) score == 2) age = 6;
        if((int) score == 3) age = 7;
        if((int) score == 4) age = 9;
        if((int) score == 5) age = 10;
        if((int) score == 6) age = 11;
        if((int) score == 7) age = 12;
        if((int) score == 8) age = 13;
        if((int) score == 9) age = 14;
        if((int) score == 10) age = 15;
        if((int) score == 11) age = 17;
        if((int) score == 12) age = 17;
        if((int) score == 13) age = 18;
        if((int) score == 14) age = 24;

        return age;
    }
}