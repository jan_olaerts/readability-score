import java.util.Scanner;

class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // put your code here

        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();

        double d = Math.sqrt(Math.pow(b,2) - 4*a*c);
        double x1 = (-b +d)/(2*a);
        double x2 = (-b -d)/(2*a);

        if(x1 < x2) {
            System.out.println(x1);
            System.out.println(x2);
        } else {
            System.out.println(x2);
            System.out.println(x1);
        }
    }
}