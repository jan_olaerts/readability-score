import java.util.Scanner;

class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // put your code here
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        double p = (a+b+c) / 2.0;

        double area = Math.sqrt(p*(p-a)*(p-b)*(p-c));
        System.out.println(area);
    }
}